import 'dart:convert';

import '../../models/users/user.dart';
import 'package:get_storage/get_storage.dart';

class StorageLocal {
  static final getStorge = GetStorage();
  static void setSP(UserModel? user) async {
    if (user == null) return;
    var userInfo = jsonEncode(user);
    getStorge.write("InfoLogin", userInfo);
  }

  static Future<UserModel> GetSP(UserModel user) async {
    var jsonInfo = getStorge.read("InfoLogin");
    Map<String, dynamic> userMap = jsonDecode(jsonInfo);
    return UserModel.fromMap(userMap);
  }
}
