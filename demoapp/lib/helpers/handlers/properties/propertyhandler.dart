import 'package:demoapp/helpers/sql_helper.dart';
import 'package:demoapp/models/propertys/property_dto.dart';
import '../../../models/propertys/picture_dto.dart';

class PropertyHandler {
  var dbHelper = DBHelper();
  Future<List<Property>> getObjects(int pageIndex, int pageSize) async {
    var dbClient = await dbHelper.database;
    final String query =
        "SELECT * FROM Objects Order by ID desc Limit ${pageIndex * pageSize},$pageSize";
    var res = await dbClient.rawQuery(query);
    // if (res.isNotEmpty) {
    //   return res.map((e) => Property.fromMap(e)).toList();
    // }
    return res.map((e) => Property.fromMap(e)).toList();
  }

  Future<Property> getPropertyDetail(int id) async {
    var dbClient = await dbHelper.database;
    final String query = "SELECT * FROM Objects Where ID=${id}";
    var res = await dbClient.rawQuery(query);
    return res.map((e) => Property.fromMap(e)).first;
  }

  Future<List<Picture>?> getFirstPicture(List<int> objectIds) async {
    String objs = objectIds.toString();
    objs = objs.replaceFirst('[', '(');
    objs = objs.replaceFirst(']', ')');
    var query = "SELECT p.ID " +
        ", o.Id as ObjectId " +
        ", o.ProjectId " +
        ", p.PicNr " +
        ", p.Publish " +
        ", p.DescEN as Description " +
        ", p.RowVer " +
        "FROM Pictures p " +
        "JOIN Objects o on o.Id = p.ObjectId AND o.ProjectID = 0 " +
        "WHERE p.ObjectID IN $objs and p.PicNr = 0 " +
        "UNION " +
        "SELECT p.ID " +
        ", o.ID as ObjectId " +
        ", o.ProjectId " +
        ", p.PicNr " +
        ", p.Publish " +
        ", p.DescEN as Description " +
        ", p.RowVer " +
        "FROM ConstructionPictures p " +
        "JOIN Objects o on o.ProjectID = p.ProjectID " +
        "WHERE o.Id IN $objs and p.PicNr = 0";
    var dbClient = await dbHelper.database;
    var res = await dbClient.rawQuery(query);
    return res.map((e) => Picture.fromMap(e)).toList();
  }
}
