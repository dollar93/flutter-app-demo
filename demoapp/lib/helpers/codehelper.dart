import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CodeHelper {
  static NumberFormat oCcy = NumberFormat("#,##0", "en_US");
  static String getNumberFormat(double value, {bool isShowCurrency = false}) {
    if (value == null) return "0";
    return "${oCcy.format(value).toString()}${isShowCurrency == true ? "€" : ""}";
  }

  static String getGoalName(int doel) {
    return doel == 0 ? "For rent" : "For sale";
  }

  static String getStatusName(int status) {
    switch (status) {
      case 0:
        return "Prospec mandate";
      case 1:
        return "Active mandate";
      case 2:
        return "Pedding";
      case 3:
        return "Cancel";
      default:
        return "Active";
    }
  }

  static Color? getStatusColor(int status) {
    switch (status) {
      case 0:
        return Color.fromARGB(255, 255, 134, 59);
      case 1:
        return Colors.green;
      case 2:
        return Colors.grey[600];
      case 3:
        return Colors.red;
      default:
        return Colors.green;
    }
  }
}
