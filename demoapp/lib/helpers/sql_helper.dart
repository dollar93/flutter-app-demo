import 'package:demoapp/models/users/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io' as io;

class DBHelper {
  static Database? _db;

  static const String dbName = 'localdb.db';
  static const int version = 1;
  String urlSyncFile = 'http://download.omnicasa.com/dotd/localdb.db';
  static const int pageSize = 30;

  Future<Database> get database async => _db ??= await initDb();

  Future<Database> initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, dbName);
    print('ApplicationDB: ' + path);
    var db = await openDatabase(path, version: version);
    return db;
  }

  Future<String> get pathDBFromStorage async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    return documentsDirectory.path;
  }

  Future<UserModel?> getLoginUser(String username, String password) async {
    var dbClient = await database;
    var res = await dbClient.rawQuery("SELECT * FROM Users");

    if (res.isNotEmpty) {
      return UserModel.fromMap(res.first);
    }
    return null;
  }
}
