import 'dart:io' as io;

import 'package:flutter/material.dart';

class FileHelper {
  static Future<bool> checkFileInLocal(String pathFile) async {
    return await io.File(pathFile).exists();
  }

  static Stack fallbackImage() {
    return Stack(
      children: [
        CircleAvatar(
            backgroundColor: Colors.brown.shade800,
            child: const Text('Not found'))
      ],
    );
  }
}
