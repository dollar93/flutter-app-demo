import 'package:flutter/material.dart';

class NavigationPage {
  static void pushNavigation(
      BuildContext context, String page, Object parameters) {
    // Get.offAllNamed(page);
    Navigator.pushNamed(context, page, arguments: parameters);
  }

  static void popNavigation(BuildContext context) {
    Navigator.pop(context);
  }
}
