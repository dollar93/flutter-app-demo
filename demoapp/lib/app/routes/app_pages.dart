import 'package:demoapp/app/modules/home/homepage/home_page.dart';
import 'package:demoapp/app/modules/login/login_page.dart';
import 'package:demoapp/app/modules/signup/signup_page.dart';
import 'package:demoapp/app/modules/splash/splash_page.dart';
import 'package:demoapp/app/modules/welcome/welcomepage.dart';
import 'package:get/route_manager.dart';

import 'app_routes.dart';

class AppPages {
  AppPages._();
  static const INITIAL = Routes.SPLASH;
  static final routes = [
    GetPage(name: Routes.SPLASH, page: () => const SplashPage()),
    GetPage(name: Routes.HOME, page: () => const HomePage()),
    GetPage(name: Routes.LOGIN, page: () => const LoginPage()),
    GetPage(name: Routes.SIGNUP, page: () => const SignUpPage()),
    GetPage(name: Routes.WELCOME, page: () => const WelcomePage()),
    // GetPage(
    //     name: Routes.PROPERTYDETAIL, page: () => const PropertyDetailPage()),
  ];
}
