abstract class Routes {
  Routes._();
  static const HOME = '/';
  static const SPLASH = '/splash';
  static const WELCOME = '/welcome';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';

  // property module
  static const PROPERTYDETAIL = '/propertydetail';
}
