import 'package:demoapp/components/background.dart';
import 'package:flutter/material.dart';

class PersonPage extends StatefulWidget {
  const PersonPage({Key? key}) : super(key: key);

  @override
  State<PersonPage> createState() => _PersonPageState();
}

class _PersonPageState extends State<PersonPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Backgound(
        child: SingleChildScrollView(
      child: SafeArea(child: Text('This is person page')),
    ));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
