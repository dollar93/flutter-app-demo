import 'package:demoapp/app/modules/login/login_page.dart';
import 'package:demoapp/components/check_have_account.dart';
import 'package:flutter/material.dart';
import '../../../../constants.dart';

class SignUpButtons extends StatelessWidget {
  const SignUpButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          TextFormField(
            cursorColor: kPrimaryColor,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.text,
            onSaved: (name) {},
            decoration: const InputDecoration(
                hintText: "Your fullname",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.info),
                )),
          ),
          const SizedBox(
            height: defaultPadding,
          ),
          TextFormField(
            cursorColor: kPrimaryColor,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.phone,
            onSaved: (phone) {},
            decoration: const InputDecoration(
                hintText: "Your phone",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.phone),
                )),
          ),
          const SizedBox(
            height: defaultPadding,
          ),
          TextFormField(
            cursorColor: kPrimaryColor,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.emailAddress,
            onSaved: (email) {},
            decoration: const InputDecoration(
                hintText: "Your email address",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.email),
                )),
          ),
          const SizedBox(
            height: defaultPadding,
          ),
          TextFormField(
            cursorColor: kPrimaryColor,
            textInputAction: TextInputAction.done,
            obscureText: true,
            onSaved: (password) {},
            decoration: const InputDecoration(
                hintText: "Your password",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.lock),
                )),
          ),
          const SizedBox(
            height: defaultPadding,
          ),
          Hero(
              tag: "signup_btn",
              child: ElevatedButton(
                  child: Text("Sign up".toUpperCase()), onPressed: () => {})),
          const SizedBox(
            height: defaultPadding * 4,
          ),
          AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const LoginPage();
                }));
              })
        ],
      ),
    );
  }
}
