import 'package:demoapp/components/background.dart';
import 'package:demoapp/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'components/signup_buttons.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Backgound(
        child: SingleChildScrollView(
      child: SafeArea(child: Responsive(mobile: MobileSignUp())),
    ));
  }
}

class MobileSignUp extends StatelessWidget {
  const MobileSignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Sign up".toUpperCase(),
          style: const TextStyle(
              color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        Row(
          children: [
            const Spacer(),
            Expanded(child: SvgPicture.asset("assets/icons/signup.svg")),
            const Spacer()
          ],
        ),
        Row(
          children: const [
            Spacer(),
            Expanded(flex: 8, child: SignUpButtons()),
            Spacer()
          ],
        )
      ],
    );
  }
}
