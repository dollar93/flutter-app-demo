import 'dart:io';
import 'package:demoapp/app/modules/welcome/components/welcome_image.dart';
import 'package:demoapp/components/background.dart';
import 'package:demoapp/models/users/user.dart';
import 'package:demoapp/responsive.dart';
import 'package:flowder/flowder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:path/path.dart';
import '../../../helpers/files/file_helper.dart';
import '../../../helpers/sql_helper.dart';
import '../../../helpers/user_storagelocal/storageinfo.dart';
import '../../routes/app_routes.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  final getStorage = GetStorage();
  int percently = 0;
  int totalValue = 100;
  String downloadingTitle = 'Downloading... ';
  String downloadedTitle = 'Download successfuly. Redirecting to HomePage...';
  String processingTitle = '';
  @override
  void initState() {
    super.initState();
    percently = 0;
    processingTitle = "$downloadingTitle${percently.toString()}%";
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Backgound(
      child: SingleChildScrollView(
        child: SafeArea(
            child: Responsive(
          mobile: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const WelComeImage(),
              CircularProgressIndicator.adaptive(
                value: percently / totalValue,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(processingTitle),
            ],
          ),
        )),
      ),
    );
  }

  void checkLogin() {
    if (getStorage.read("userid") != null &&
        int.parse(getStorage.read("userid")) > 0) {
      firstSyncDB();
      // Future.delayed(const Duration(microseconds: 2000), () {
      //   Get.offAllNamed(Routes.HOME);
      // });
    } else {
      Future.delayed(Duration.zero, () async {
        Get.offAllNamed(Routes.WELCOME);
      });
    }
  }

  Future firstSyncDB() async {
    var dbHelper = DBHelper();
    var pathStorage = await dbHelper.pathDBFromStorage;
    final dbPathFull = join(pathStorage, "localdb.db");

    if (await FileHelper.checkFileInLocal(dbPathFull)) {
      Get.offAllNamed(Routes.HOME);
      return;
    }
    final downloaderUtils = DownloaderUtils(
      progressCallback: (current, total) {
        setState(() {
          percently = calcranks(current / total * totalValue);
          processingTitle = "$downloadingTitle${percently.toString()}%";
        });
      },
      file: File(dbPathFull),
      progress: ProgressImplementation(),
      onDone: () => {downloaded()},
      deleteOnCancel: true,
    );

    var core = await Flowder.download(dbHelper.urlSyncFile, downloaderUtils);
  }

  void downloaded() async {
    final getStorage = GetStorage();
    var userInfo = await DBHelper().getLoginUser('', '');
    StorageLocal.setSP(userInfo);
    setState(() {
      processingTitle = downloadedTitle;
      Future.delayed(const Duration(seconds: 2), () async {
        Get.offAllNamed(Routes.HOME);
      });
    });
  }

  int calcranks(ranks) {
    return (ranks).round();
  }
}
