import 'package:demoapp/components/background.dart';
import 'package:demoapp/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../constants.dart';
import 'components/login_button.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Backgound(
      child: SingleChildScrollView(
        child: SafeArea(child: Responsive(mobile: MobileLogin())),
      ),
    );
  }
}

class MobileLogin extends StatelessWidget {
  const MobileLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Login".toUpperCase(),
            style: const TextStyle(
                color: Colors.black87, fontWeight: FontWeight.bold)),
        const SizedBox(
          height: defaultPadding * 2,
        ),
        Row(
          children: [
            const Spacer(),
            Expanded(
                flex: 8, child: SvgPicture.asset("assets/icons/login.svg")),
            const Spacer()
          ],
        ),
        const SizedBox(
          height: defaultPadding * 2,
        ),
        Row(
          children: const [
            Spacer(),
            Expanded(
              child: LoginButtons(),
              flex: 8,
            ),
            Spacer()
          ],
        )
      ],
    );
  }
}
