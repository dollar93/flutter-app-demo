import 'package:demoapp/app/modules/signup/signup_page.dart';
import 'package:demoapp/components/check_have_account.dart';
import 'package:demoapp/constants.dart';
import 'package:demoapp/helpers/sql_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../../../routes/app_routes.dart';

class LoginButtons extends StatefulWidget {
  const LoginButtons({Key? key}) : super(key: key);

  @override
  State<LoginButtons> createState() => _State();
}

class _State extends State<LoginButtons> {
  final _conUserName = TextEditingController();
  final _conPassword = TextEditingController();
  var dbHelper;
  final getStorge = GetStorage();

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(
      children: [
        TextFormField(
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.emailAddress,
          cursorColor: kPrimaryColor,
          controller: _conUserName,
          onSaved: (email) {},
          decoration: const InputDecoration(
            hintText: "Your account...",
            prefixIcon: Padding(
              padding: EdgeInsets.all(defaultPadding),
              child: Icon(Icons.person),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: defaultPadding),
          child: TextFormField(
            textInputAction: TextInputAction.done,
            obscureText: true,
            cursorColor: kPrimaryColor,
            controller: _conPassword,
            decoration: const InputDecoration(
                hintText: "Your password...",
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.lock),
                )),
          ),
        ),
        const SizedBox(
          height: defaultPadding,
        ),
        Hero(
            tag: "login_btn",
            child: ElevatedButton(
                child: Text("Login".toUpperCase()), onPressed: login)),
        const SizedBox(
          height: defaultPadding * 4,
        ),
        AlreadyHaveAnAccountCheck(press: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const SignUpPage();
          }));
        })
      ],
    ));
  }

  void login() async {
    String usName = _conUserName.text;
    String passwd = _conPassword.text;
    Get.offAllNamed(Routes.SPLASH);
    // UserModel userModel = UserModel(123, usName, 'dotd', 'email');
    // setSP(userModel);
    // await dbHelper.getLoginUser(usName, passwd).then((userData) {
    //   if (userData != null) {
    //     setSP(userData).whenComplete(() => {
    //           // Navigator.pushAndRemoveUntil(
    //           //     context,
    //           //     MaterialPageRoute(builder: (_) => const HomePage()),
    //           //     (Route<dynamic> route) => false)
    //           Get.offAllNamed(Routes.SPLASH)
    //         });
    //   } else {}
    // });
  }
}
