import 'package:curved_labeled_navigation_bar/curved_navigation_bar.dart';
import 'package:curved_labeled_navigation_bar/curved_navigation_bar_item.dart';
import 'package:demoapp/app/modules/personpage/person_page.dart';
import 'package:demoapp/app/modules/propertypage/property_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _page = 0;
  final GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

  final List screens = [
    {"screen": const PropertyPage(), "title": "Property page"},
    {"screen": const PersonPage(), "title": "Person page"},
    {"screen": const PersonPage(), "title": "Person page"},
    {"screen": const PersonPage(), "title": "Person page"},
    {"screen": const PersonPage(), "title": "Person page"}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: _page,
          items: const [
            CurvedNavigationBarItem(
              child: Icon(Icons.real_estate_agent_sharp),
              label: 'Property',
            ),
            CurvedNavigationBarItem(
              child: Icon(Icons.view_agenda),
              label: 'Agenda',
            ),
            CurvedNavigationBarItem(
              child: Icon(Icons.person),
              label: 'Client',
            ),
            CurvedNavigationBarItem(
              child: Icon(Icons.task),
              label: 'Task',
            ),
            CurvedNavigationBarItem(
              child: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.grey,
          backgroundColor: Colors.blueAccent,
          animationCurve: Curves.easeInOut,
          animationDuration: const Duration(milliseconds: 200),
          onTap: (index) {
            setState(() {
              _page = index;
            });
          },
          letIndexChange: (index) => true,
        ),
        body: screens[_page]['screen']);
  }
}
