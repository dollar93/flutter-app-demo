import 'package:demoapp/AppUI.dart';
import 'package:demoapp/constants.dart';
import 'package:demoapp/helpers/codehelper.dart';
import 'package:demoapp/models/propertys/property_dto.dart';
import 'package:flutter/material.dart';
import '../../../helpers/files/file_helper.dart';
import '../../../helpers/handlers/properties/propertyhandler.dart';
import '../../../models/requestparams/requestparam.dart';

class PropertyDetailPage extends StatefulWidget {
  final RequestParamObject detail;
  const PropertyDetailPage({Key? key, required this.detail}) : super(key: key);

  @override
  State<PropertyDetailPage> createState() => _PropertyDetailPageState();
}

class _PropertyDetailPageState extends State<PropertyDetailPage> {
  Property? property;
  var propertyHandler = PropertyHandler();
  @override
  void initState() {
    getDetail(widget.detail.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Property detail'),
      ),
      body: Column(
        children: [
          Stack(children: [
            property?.cloudPictureUrl == null
                ? FileHelper.fallbackImage()
                : Image.network(property?.cloudPictureUrl ?? ""),
            Positioned(
              bottom: 0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 25,
                color: Colors.grey.withOpacity(0.8),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'ID: ${property?.id}',
                        style: AppUIStyle.textStyleWhitePrimaryBold(),
                      )
                    ],
                  ),
                ),
              ),
            )
          ]),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 5),
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    property?.ident ?? "",
                    style: AppUIStyle.textStyleBlackPrimaryBold(),
                  ),
                ),
                const SizedBox(height: defaultPadding / 4),
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          child: Text(
                            property?.straat ?? "",
                            style: AppUIStyle.textStyleBlackPrimaryNormal(),
                          ),
                        ),
                        const SizedBox(height: defaultPadding / 4),
                        SizedBox(
                          child: Text(
                            "Manager: Britand",
                            style: AppUIStyle.textStyleBlackPrimaryNormal(),
                          ),
                        ),
                        const SizedBox(height: defaultPadding / 4),
                        SizedBox(
                          child: Text(
                            "Site: Heylen",
                            style: AppUIStyle.textStyleBlackPrimaryNormal(),
                          ),
                        )
                      ],
                    ),
                    const Spacer(),
                    SizedBox(
                      child: Text(
                        CodeHelper.getNumberFormat(property?.price ?? 0,
                            isShowCurrency: true),
                        style: AppUIStyle.textStyleBlueLargePrimaryBold(),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: defaultPadding / 4),
                Row(
                  children: [
                    Container(
                        // Add the line below
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          // border: Border.all(
                          //     color: Colors.green, width: 2.0)
                        ),
                        child: Container(
                          color:
                              CodeHelper.getStatusColor(property?.status ?? 0),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              CodeHelper.getStatusName(property?.status ?? 0),
                              style: AppUIStyle.textStyleWhitePrimaryBold(),
                            ),
                          ),
                        )),
                    const SizedBox(width: defaultPadding / 2),
                    Container(
                        // Add the line below
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          // border: Border.all(
                          //     color: Colors.green, width: 2.0)
                        ),
                        child: Container(
                          color: Color.fromRGBO(207, 222, 248, 1),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              CodeHelper.getGoalName(property?.doel ?? 0),
                              style: AppUIStyle.textStyleBluePrimaryBold(),
                            ),
                          ),
                        ))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void getDetail(int id) async {
    var detail = await propertyHandler.getPropertyDetail(id);
    setState(() {
      property = detail;
      property?.cloudPictureUrl = widget.detail.picture;
    });
  }
}
