import 'dart:math';
import 'package:demoapp/app/modules/propertypage/propertydetail_page.dart';
import 'package:demoapp/constants.dart';
import 'package:demoapp/helpers/handlers/properties/propertyhandler.dart';
import 'package:demoapp/helpers/sql_helper.dart';
import 'package:demoapp/models/propertys/property_dto.dart';
import 'package:demoapp/models/requestparams/requestparam.dart';
import 'package:flutter/material.dart';
import '../../../AppUI.dart';
import '../../../helpers/codehelper.dart';
import '../../../helpers/files/file_helper.dart';

class PropertyPage extends StatefulWidget {
  const PropertyPage({Key? key}) : super(key: key);

  @override
  State<PropertyPage> createState() => _PropertyPageState();
}

class _PropertyPageState extends State<PropertyPage> {
  var propertyHandler = PropertyHandler();
  List<Property> models = [];
  int pageIndex = 1;
  late ScrollController controller;
  bool isLoadingMore = true;
  bool haveNextPage = true;
  List<String> listPictures = [];

  @override
  void initState() {
    super.initState();
    initPictures();
    loadFirstData(pageIndex);
    controller = ScrollController()..addListener(loadMoreData);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Property page'),
        actions: [IconButton(onPressed: () => {}, icon: const Icon(Icons.add))],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: models.length,
              controller: controller,
              itemBuilder: (_, index) => GestureDetector(
                onTap: () => {
                  // NavigationPage.pushNavigation(
                  //     context, Routes.PROPERTYDETAIL, getParam(index))
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          PropertyDetailPage(detail: getParam(index))))
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          (models[index].cloudPictureUrl?.isNotEmpty == false)
                              ? FileHelper.fallbackImage()
                              : Image.network(
                                  models[index].cloudPictureUrl ?? ""),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                // Add the line below
                                clipBehavior: Clip.hardEdge,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  // border: Border.all(
                                  //     color: Colors.green, width: 2.0)
                                ),
                                child: Container(
                                  color: models[index].statusColor,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Text(
                                      models[index].statusName ?? "",
                                      style: AppUIStyle
                                          .textStyleWhitePrimaryBold(),
                                    ),
                                  ),
                                )),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: defaultPadding / 2,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Price: ${CodeHelper.getNumberFormat(models[index].price, isShowCurrency: true)}",
                            textAlign: TextAlign.left,
                            style: AppUIStyle.textStyleBluePrimaryBold(),
                          ),
                          const Spacer(),
                          Text(
                            CodeHelper.getGoalName(models[index].doel),
                            textAlign: TextAlign.left,
                            style: AppUIStyle.textStyleBluePrimaryBold(),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: defaultPadding / 4,
                      ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Flexible(
                            child: Text(models[index].straat ?? "",
                                style: AppUIStyle.textStyleGrayPrimary(),
                                textAlign: TextAlign.left),
                          )),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Flexible(
                          child: Text(
                            models[index].ident ?? "",
                            textAlign: TextAlign.left,
                            style: AppUIStyle.textStyleBlackPrimaryBold(),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                              "${models[index].bathRoom.toString()} bathrooms - ",
                              style: AppUIStyle.textStyleGrayPrimary(),
                              textAlign: TextAlign.left),
                          Text(
                              "${models[index].bedroom.toString()} bedrooms - ",
                              style: AppUIStyle.textStyleGrayPrimary(),
                              textAlign: TextAlign.left),
                          Text("${models[index].parking.toString()} parkings",
                              style: AppUIStyle.textStyleGrayPrimary(),
                              textAlign: TextAlign.left),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (isLoadingMore == true)
            const Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }

  void loadMoreData() {
    if (haveNextPage &&
        !isLoadingMore &&
        controller.position.extentAfter < 300) {
      setState(() {
        isLoadingMore = true;
      });
      pageIndex++;
      loadFirstData(pageIndex);
    }
  }

  void loadFirstData(int index) async {
    var res = await propertyHandler.getObjects(index, DBHelper.pageSize);
    if (res.isEmpty) {
      setState(() {
        isLoadingMore = haveNextPage = false;
      });
      return;
    }
    setState(() {
      models.addAll(res);
      isLoadingMore = false;
    });
    setPictureForProperties(res);
  }

  void setPictureForProperties(List<Property> properties) async {
    // var propertyIds = properties.map((e) => e.id).toList();
    // var pics = await propertyHandler.getFirstPicture(propertyIds);
    // if (pics == null) {
    //   return;
    // }
    setState(() {
      properties.forEach((property) {
        // if (property.cloudPictureUrl?.isEmpty == false) {
        //   var picFirstByPropertyId = pics.firstWhereOrNull((element) =>
        //       element.objectId == property.id && element.picNr == 0);
        //   if (picFirstByPropertyId != null) {
        //     property.cloudPictureUrl =
        //         "https://i.ytimg.com/vi/vcweoQ5oXUQ/maxresdefault.jpg";
        //   }
        // }
        property.statusName = CodeHelper.getStatusName(property.status);
        property.statusColor = CodeHelper.getStatusColor(property.status);
        property.cloudPictureUrl = listPictures[randomPicture()];
      });
    });
  }

  void initPictures() {
    listPictures.add('https://i.ytimg.com/vi/vcweoQ5oXUQ/maxresdefault.jpg');
    listPictures.add(
        'http://cdn.reatimes.vn/mediav2/upload/userfiles/images/LUONG%20KIM/2401/thi-truong-bds.jpg');
    listPictures.add(
        'https://xuhuongtaichinh.com/wp-content/uploads/2021/07/bat-dong-san-ha-noi.jpg');
    listPictures.add(
        'https://cdn.vietnambiz.vn/171464876016439296/2020/10/2/13-1598579910569300721072-160161328157655711394.jpg');
    listPictures.add(
        'https://cdn.thoibaonganhang.vn/stores/news_dataimages/thanhlm/052022/01/13/3717_nhung-luu-y-khi-mua-nha-o-thuong-mai.jpg');
    listPictures.add(
        'https://cdn.vietnambiz.vn/171464876016439296/2021/9/30/hehiphu-yen-3-16329705348541244429754.jpg');
    listPictures.add(
        'http://cdn.reatimes.vn/mediav2/upload/userfiles2021/images/vuhong/Kich-ban-thi-truong(1).jpeg');
    listPictures.add(
        'https://media1.nguoiduatin.vn/thumb_x640x384/media/tran-thu-thao/2022/01/24/gia-bat-dong-san-nguoiduatinvn-7.jpeg');
    listPictures.add(
        'https://file4.batdongsan.com.vn/crop/600x315/2023/02/26/20230226140510-1870_wm.jpeg');
    listPictures.add(
        'https://file4.batdongsan.com.vn/crop/600x315/2023/01/29/20230129140135-5119_wm.jpg');
  }

  int randomPicture() {
    var rng = Random();
    return rng.nextInt(10);
  }

  RequestParamObject getParam(int index) {
    // return {'id': models[index].id};
    return RequestParamObject(models[index].id, models[index].cloudPictureUrl);
  }
}
