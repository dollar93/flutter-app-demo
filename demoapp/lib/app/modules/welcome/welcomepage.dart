import 'package:demoapp/components/background.dart';
import 'package:flutter/material.dart';
import '../../../responsive.dart';
import 'components/welcome_buttons.dart';
import 'components/welcome_image.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Backgound(
      child: SingleChildScrollView(
        child: SafeArea(
            child: Responsive(
          mobile: MobileWelcomeScreen(),
        )),
      ),
    );
  }
}

class MobileWelcomeScreen extends StatelessWidget {
  const MobileWelcomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const WelComeImage(),
        Row(
          children: const [
            Spacer(),
            Expanded(flex: 8, child: LoginAndSignupBtn()),
            Spacer()
          ],
        )
      ],
    );
  }
}
