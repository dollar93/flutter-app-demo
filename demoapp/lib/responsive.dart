import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget? table;
  final Widget? desktop;
  const Responsive({Key? key, required this.mobile, this.table, this.desktop})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return mobile;
  }
}
