import 'package:flutter/material.dart';

class AppUIStyle {
  static TextStyle textStyleBluePrimaryBold() {
    return const TextStyle(
        color: Colors.blue, fontSize: 12, fontWeight: FontWeight.bold);
  }

  static TextStyle textStyleBlueLargePrimaryBold() {
    return const TextStyle(
        color: Colors.blue, fontSize: 14, fontWeight: FontWeight.bold);
  }

  static TextStyle textStyleWhitePrimaryBold() {
    return const TextStyle(
        color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold);
  }

  static TextStyle textStyleGrayPrimary() {
    return const TextStyle(color: Colors.grey, fontSize: 12);
  }

  static TextStyle textStyleBlackPrimaryBold() {
    return const TextStyle(
        color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold);
  }

  static TextStyle textStyleBlackPrimaryNormal() {
    return const TextStyle(color: Colors.black, fontSize: 12);
  }
}
