import 'dart:ui';

class Property {
  int id;
  int? cityID;
  String? cityName;
  String? cityName2;
  double price;
  String? boxNr;
  int doel;
  String? straat;
  int? cityPostCode;
  String? ident;
  int status;
  String? statusName;
  Color? statusColor;
  int? subStatus;
  String? subStatusName;
  String? typeDecriptionEN;
  String? typeDecriptionNL;
  String? typeDecriptionFR;
  String? address2;
  String? localImageFilePath;
  int? bathRoom;
  int? bedroom;
  int? parking;
  String? cloudPictureUrl =
      "https://images.ctfassets.net/zykafdb0ssf5/68qzkHjCboFfCsSxV2v9S6/4da75033db02c1339de2a3effb461f7a/missing.png";

  Property(
      this.id,
      this.cityID,
      this.cityName,
      this.cityName2,
      this.price,
      this.straat,
      this.cityPostCode,
      this.ident,
      this.status,
      this.statusName,
      this.statusColor,
      this.subStatus,
      this.typeDecriptionEN,
      this.typeDecriptionNL,
      this.typeDecriptionFR,
      this.address2,
      this.boxNr,
      this.doel,
      this.bathRoom,
      this.bedroom,
      this.parking,
      this.localImageFilePath);
  Property.fromMap(Map<String, dynamic> res)
      : id = res["ID"],
        cityID = res["CityID"],
        cityName = res["CityName"],
        cityName2 = res["CityName2"],
        price = res["Prijs"],
        straat = res["Straat"],
        cityPostCode = res["CityPostCode"],
        ident = res["Ident"],
        status = res["Status"],
        subStatus = res["SubStatus"],
        typeDecriptionEN = res["TypeDecriptionEN"],
        typeDecriptionNL = res["TypeDecriptionNL"],
        typeDecriptionFR = res["TypeDecriptionFR"],
        address2 = res["Address2"],
        boxNr = res["BoxNr"],
        doel = res["Doel"],
        bathRoom = res["NrBadkamers"],
        bedroom = res["NrSlaapkamers"],
        parking = res["NrParkings"];
  // Map<String, Object?> toMap() {
  //   return {
  //     'ID': id,
  //     'CityID': cityID,
  //     'CityName': cityName,
  //     'CityName2': cityName2,
  //     'Prijs': price,
  //     'Straat': straat,
  //     'CityPostCode': cityPostCode,
  //     'Ident': ident,
  //     'Status': status,
  //     'SubStatus': subStatus,
  //     'TypeDecriptionEN': typeDecriptionEN,
  //     'TypeDecriptionNL': typeDecriptionNL,
  //     'TypeDecriptionFR': typeDecriptionFR,
  //     'Address2': address2,
  //     'BoxNr': boxNr,
  //     'Doel': doel,
  //   };
  // }
}
