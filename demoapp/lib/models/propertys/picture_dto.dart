class Picture {
  int id;
  int objectId;
  int projectId;
  int picNr;
  int publish;
  String description;
  String rowVer;
  Picture(this.id, this.objectId, this.projectId, this.picNr, this.publish,
      this.description, this.rowVer);
  Map<String, Object?> toMap() {
    return {
      'Id': id,
      'ObjectId': objectId,
      'ProjectId': projectId,
      'PicNr': picNr,
      'Publish': publish,
      'Description': description,
      'RowVer': rowVer,
    };
  }

  Picture.fromMap(Map<String, dynamic> res)
      : id = res['Id'],
        objectId = res['ObjectId'],
        projectId = res['ProjectId'],
        picNr = res['PicNr'],
        publish = res['Publish'],
        description = res['Description'],
        rowVer = res['RowVer'];
}
