class UserModel {
  String? name;
  String? email;
  String? shortname;
  int? userid;

  UserModel(this.userid, this.name, this.shortname, this.email);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'UserID': userid,
      'Name': name,
      'Email': email,
      'ShortName': shortname
    };
    return map;
  }

  UserModel.fromMap(Map<String, dynamic> map) {
    userid = map['UserID'];
    name = map['Name'];
    email = map['Email'];
    shortname = map['ShortName'];
  }
}
