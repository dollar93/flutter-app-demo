class RequestParamObject {
  int id;
  String? picture;
  RequestParamObject(this.id, this.picture);
  RequestParamObject.fromMap(Map<String, dynamic> res)
      : id = res["id"],
        picture = res["picture"];
}
