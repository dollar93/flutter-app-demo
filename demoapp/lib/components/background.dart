import 'package:flutter/material.dart';

class Backgound extends StatelessWidget {
  final Widget child;
  final String topImage, bottomImage;
  const Backgound(
      {Key? key,
      required this.child,
      this.topImage = "assets/images/main_top.png",
      this.bottomImage = "assets/images/main_bottom.png"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  topImage,
                  width: 120,
                )),
            Positioned(
              bottom: 0,
              left: 0,
              child: Image.asset(bottomImage, width: 60),
            ),
            SafeArea(child: child)
          ],
        ),
      ),
    );
  }
}
